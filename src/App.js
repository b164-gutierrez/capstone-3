import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ErrorPage from './pages/Error';
import { UserProvider } from './UserContext';
import OrderHistory from './pages/OrderHistory';
import AdminView from './components/AdminView';
import './App.css';

function App() {

// React Context is nothing but a global state to the app. It is a way to make particular data available to all components no matter how they are nested.

    // State hook for the user state that defined here for a global scope.
    // Initialized as an object with properties from the local storage/
    // This will be used to store the user information and will be used for validating if a user is logged in on the app or not.

    const [user, setUser] = useState({
        // email: localStorage.getItem('email')
        id: null,
        isAdmin: null
    });

    // Create a function for clearing the localStorage on logout.

    const unsetUser = () => {
        localStorage.clear()
    };

    // useEffect(() => {
    //     console.log(user)
    // }, [user])

    useEffect(() => {

        fetch('https://morning-atoll-63884.herokuapp.com/api/users/details', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data._id !== 'undefined') {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            } else {
                setUser({
                    id: null,
                    isAdmin: null
                })
            }
        })

    }, [])
    // [] => if the array is empty it will run only once.


  return (
    <UserProvider value= {{user, setUser, unsetUser}}>

        <Router>
            <AppNavBar/>
                <Container>
                  <Routes>

                    <Route path='/' element={<Home/>} />
                    <Route path='/products' element={<Products/>} />
                    <Route path='/products/:productId' element={<ProductView/>} />
                    <Route path='/login' element={<Login/>} />
                    <Route path='/register' element={<Register/>} />
                    <Route path='/logout' element={<Logout/>} />
                    <Route path='/orderhistory' element={<OrderHistory/>} />        
                    <Route path='/adminview' element={<AdminView/>} />
                    <Route path='*' element={<ErrorPage/>} />

                  </Routes>
                </Container>
        </Router>
    </UserProvider>
  );
};

export default App;
   

            







