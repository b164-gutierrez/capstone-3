import { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Card, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

// useNavigate same as Navigate hook => redirect page


export default function ProductView() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	// The useParams hook allows us to retirevethe courseId passed via the URL.
	const { productId } = useParams();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const purchase = (productId) => {

		fetch('https://morning-atoll-63884.herokuapp.com/api/users/checkout', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')} `
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data) //possible response = true or false

			if (data === true) {
				Swal.fire({
					title: 'Purchase Successful!',
					icon: 'success',
					text: 'Congratulation on your New Motorcycle!.'
				})

				navigate('/products');

			} else {
				Swal.fire({
					title: 'Something went wrong!',
					icon: 'error',
					text: 'Please try again.'
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId)
		fetch(`https://morning-atoll-63884.herokuapp.com/api/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			// (data) contains data from the backend (API)

			// use dot notation ex:(data.name) to access properties of (data)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

	}, [productId])

	return(

		<Container>
			<Row>
				<Col>
					<Card>
						<Card.Body className='text-center'>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							{
								user.id !== null ?
								<Button variant='primary' onClick={() => purchase(productId)}
								>Purchase</Button>
								:
								<Link className='btn btn-danger' to='/login'>Log in to Purchase</Link>
							}
							
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		)
}
							



							



