import HistoryCard from '../components/HistoryCard';
import {Fragment, useEffect, useState} from 'react';



export default function History() {
	
	const [orders, setOrders] = useState([])

		useEffect(() => {
			
		 fetch('https://morning-atoll-63884.herokuapp.com/api/users/order', {
            headers: {
            	'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
			})	

			.then(res => res.json())
			.then(data => {
				console.log(data)

				

				setOrders(data.map(history => {
					return (
					<HistoryCard key={orders.transactionDate} historyProp={history}/>
					)

				}))

			})
		}, [])


		return(
			<Fragment>
				<h3>Order History</h3>
				{orders}
			</Fragment>
	);
};




