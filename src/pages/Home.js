import {Fragment} from 'react';
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'


export default function() {
	return(
		<Fragment>
			<Banner />
			<Highlights />
		</Fragment>
	);
};
			
