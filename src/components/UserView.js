import { useState, useEffect } from 'react';
// import courseData from './../data/courses'
import ProductCard from './ProductCard'

export default function UserView({productProp}) {

	const [productsArr, setProductsArr] = useState([])

	// console.log(courseData)

	//on component mount/page load, useEffect will run and call our fetchData function, which runs our fetch request
	useEffect(() => {
		const products = productProp.map(product => {
			// console.log(course)
			if(product.isActive){
				return <ProductCard key={product._id} productProp={product} />
			}else{
				return null
			}
		})

		setProductsArr(products)

	}, [productProp])

	return(
		<>
			{productsArr}
		</>
	)
}
