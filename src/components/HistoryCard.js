import {useState, useEffect} from 'react';
import  { Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function HistoryCard({historyProp}) {

	const {name, productId, transactionDate, paymentStatus, _id} = historyProp

	return(

		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Transaction Date</Card.Subtitle>
				<Card.Text>{transactionDate}</Card.Text>
				<Card.Subtitle>Payment Status</Card.Subtitle>
				<Card.Text>{paymentStatus}</Card.Text>
				<Button variant='primary' as={Link} to={'/'} >Go to Home</Button>
			</Card.Body>
		</Card>
	);
};
		
				

				

	