import { Fragment, useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar(){

	const {user} = useContext(UserContext);

	return(
		<Navbar bg="light" expand="lg">
		  <Container>

		{/*create as={link} to='/ to make the Navbar.Brand (Batch-164 Course Booking) if you click this it will go to Home Page*/}

		    <Navbar.Brand as={Link} to='/'>BeeMotoshop</Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className='ml-auto'>
		      	<Nav.Link as={Link} to='/'>Home</Nav.Link>
		      	<Nav.Link as={Link} to='/products'>Products</Nav.Link>		      			      			      			      				     

		      		{/*change user.email to user.id*/}
		      		{ (user.id !== null || user.isAdmin == true) ?

		      			<Fragment>
		      			<Nav.Link as={Link} to='/orderhistory'>OrderHistory</Nav.Link>
		      			<Nav.Link as={Link} to='/logout'>Logout</Nav.Link>		     
		      			</Fragment>		      			

		      			:

		      			<Fragment>
		      				<Nav.Link as={Link} to='/register'>Register</Nav.Link>
		      				<Nav.Link as={Link} to='/login'>Login</Nav.Link>		
		      			</Fragment>

		      		}

		      	</Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>

	);
};



		    

		