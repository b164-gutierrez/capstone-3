import { Row, Col, Card } from 'react-bootstrap'

export default function Highlights() {
	return(
		<Row className='mt-3 mb-3'>
			<Col xs={12} md={4}>
				<Card className='cardHighlight p-3'>
					<Card.Body>
						<Card.Title>Road Motorcycle Catalog</Card.Title>
						<Card.Text>Check out all of the Scooter accessories that we has to offer.</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className='cardHighlight p-3'>
					<Card.Body>
						<Card.Title>Extend Service</Card.Title>
						<Card.Text>Get a peace of mind for you and your Motmot.</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className='cardHighlight p-3'>
					<Card.Body>
						<Card.Title>Pre-Qualify Now</Card.Title>
						<Card.Text>Get one step closer to owning the Motmot of your choice.</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	);
};

