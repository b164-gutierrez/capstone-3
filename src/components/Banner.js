import {Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner() {
	return(

		<Row>
			<Col className='p-5, text-center'>
				<h1>BeeMotoshop</h1>
				<p className='p-2'>See and Drive Youre Own Now!</p>
				<Button variant='primary' as={Link} to={'/products/'} >See Products</Button>
			</Col>
		</Row>
		);
};



